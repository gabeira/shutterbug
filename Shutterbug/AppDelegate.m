//
//  AppDelegate.m
//  Shutterbug
//
//  Created by CS193p Instructor.
//  Copyright (c) 2011 Stanford University. All rights reserved.
//

#import "AppDelegate.h"
#import "Reachability.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize internetConnection;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Check if there's Internet access
    internetConnection = YES;
    [self checkReachability];
    
    // Override point for customization after application launch.
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

- (void)checkReachability
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability:internetReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateInterfaceWithReachability: curReach];
}

- (void)updateInterfaceWithReachability:(Reachability *)curReach
{
    if (curReach == internetReach)
    {
        NetworkStatus netStatus = [curReach currentReachabilityStatus];
        if (netStatus == NotReachable)
        {
            //if (internetConnection == NO)// && (internetNotAvailableAlertView == nil)
                
            //{
                NSLog(@"no internet connection",nil);
                /*
                internetNotAvailableAlertView = [[UIAlertView alloc]
                                                 initWithTitle:NSLocalizedString(@"CONNECTION.ALERT_MESSAGE_TITLE", @"Title for alert")
                                                 message:NSLocalizedString(@"CONNECTION.ALERT_MESSAGE_ERROR", @"Error message when there is no internet connection")
                                                 delegate:self
                                                 cancelButtonTitle:NSLocalizedString(@"COMMON.OK", @"Label string ok for for buttons, alerts, etc.")
                                                 otherButtonTitles:nil, nil];
                
                //[internetNotAvailableAlertView show];
                 */
            UIAlertView *alertInternet = [[UIAlertView alloc]
                                          initWithTitle:@"Title for alert!"
                                          message:@"No internet available."
                                          delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil, nil];
            [alertInternet show];
            //}
            //NSLog(@"internet connection available yes",nil);
            internetConnection = NO;
        }
        else
        {
            
            //if (noInternetConnection)
            //{
                //if (internetNotAvailableAlertView != nil)
                //{
                    //[internetNotAvailableAlertView dismissWithClickedButtonIndex:0 animated:YES];
                //}
                NSLog(@"internet connection available",nil);
                internetConnection = YES;
            //}
        }
    }
}


@end
