//
//  AppDelegate.h
//  Shutterbug
//
//  Created by CS193p Instructor.
//  Copyright (c) 2011 Stanford University. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Reachability;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    Reachability *internetReach;
}

@property (strong, nonatomic) UIWindow *window;
@property (assign) BOOL internetConnection;


@end
